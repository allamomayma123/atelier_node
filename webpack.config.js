// webpack.config.js
const path = require('path');

module.exports = {
  entry: './src/index.js', // Point d'entrée de votre application
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader', // Si vous utilisez Babel pour transpiler votre code
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};
